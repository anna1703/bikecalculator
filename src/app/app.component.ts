import { Component } from '@angular/core';
import { BikeParams } from './model/bike-params';
import { BikeService } from './service/bike.service';

@Component({
  selector: 'bc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  speed: number = null;

  constructor(private bikeService: BikeService) {
  }

  processParams(params: BikeParams): void {
    this.speed = this.bikeService.calculateSpeed(params);
  }
}
