import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bc-bike-speed-card',
  templateUrl: './bike-speed-card.component.html',
  styleUrls: ['./bike-speed-card.component.scss']
})
export class BikeSpeedCardComponent implements OnInit {

  @Input() speed: number;

  constructor() {
  }

  ngOnInit() {
  }

}
