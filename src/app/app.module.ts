import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from './angular-material.module';
import { AppComponent } from './app.component';
import { BikeParamsFormCardComponent } from './bike-params-form-card/bike-params-form-card.component';
import { BikeSpeedCardComponent } from './bike-speed-card/bike-speed-card.component';

@NgModule({
  declarations: [
    AppComponent,
    BikeParamsFormCardComponent,
    BikeSpeedCardComponent
  ],
  imports: [
    AngularMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
