import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikeParams } from '../model/bike-params';

@Component({
  selector: 'bc-bike-params-form-card',
  templateUrl: './bike-params-form-card.component.html',
  styleUrls: ['./bike-params-form-card.component.scss']
})
export class BikeParamsFormCardComponent {

  form: FormGroup;

  @Output() params: EventEmitter<BikeParams> = new EventEmitter<BikeParams>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      cadence: [60, Validators.compose([Validators.required, Validators.min(0), Validators.max(720)])],
      frontTeeth: [40, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],
      rearTeeth: [11, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],
      wheelDiameter: [26, Validators.compose([Validators.required, Validators.min(10), Validators.max(30)])]
    });
  }

  submitParams(): void {
    this.params.emit(this.getParams());
  }

  private getParams(): BikeParams {
    return this.form.value as BikeParams;
  }

}
