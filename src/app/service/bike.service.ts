import { Injectable } from '@angular/core';
import { BikeParams } from '../model/bike-params';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  public static INCHES_IN_METER = 100 / 2.54;
  public static SECONDS_IN_MINUTE = 60;
  public static KM_PER_HOUR_COEF = 3.6;

  public calculateSpeed(params: BikeParams): number {
    const value: number = BikeService.KM_PER_HOUR_COEF *
      (Math.PI * this.caluclateGearInches(params) * (params.cadence / BikeService.SECONDS_IN_MINUTE)) / BikeService.INCHES_IN_METER;
    return _.round(value, 4);
  }

  private caluclateGearInches(params: BikeParams): number {
    return params.wheelDiameter * (params.frontTeeth / params.rearTeeth);
  }
}
