export class BikeParams {
  cadence: number;
  frontTeeth: number;
  rearTeeth: number;
  wheelDiameter: number;
}
